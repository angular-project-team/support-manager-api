﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Models
{
    public class SupportActionParams
    {
        public int Id { get; set; }
        public Support SupportId { get; set; }
        public SupportAction SupportActionId { get; set; }
        public string SupportActionParameterName { get; set; }

    }
}