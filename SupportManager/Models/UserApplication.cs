﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Models
{
    public class UserApplication
    {
        public int Id { get; set; }
        public User StaffId { get; set; }
        public Application ApplicationId { get; set; }
        public Status Status { get; set; }
    }
}