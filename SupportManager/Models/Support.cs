﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Models
{
    public class Support
    {
        public int Id { get; set; }
        public string SupportName { get; set; }
        public Application AppId { get; set; }
    }
}