﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Models
{
    public class SupportAction
    {
        public int Id { get; set; }

        public Support SupportId { get; set; }

        public string SupportActionName { get; set; }

        public string SupportActionUrl { get; set; }

        public string Method { get; set; }

        public string SupportActionWeight { get; set; }


    }
}