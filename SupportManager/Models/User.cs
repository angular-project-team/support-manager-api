﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Models
{
    public class User
    {
        public int Id { get; set; }
        public string StaffId { get; set; }
        public Status Status { get; set; }
        public Role Role { get; set; }

    }


    public enum Role
    {
        Admin,
        Staff
    }

    public enum Status
    {
        Inactive,
        Active
    }


}