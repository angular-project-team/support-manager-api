﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Models
{
    public class APIResponse
    {
        public string ResponseCode { get; set; }
        public string RequestId { get; set; }
        public string ResponseMessage { get; set; }
    }
}