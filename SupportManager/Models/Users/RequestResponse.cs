using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SupportManager.Models.Users
{
  public class RequestResponse
  {
    public string ResponseCode { get; set; }
    public string RequestId { get; set; }
    public string ResponseMessage { get; set; }
    public DataTable ResponseObject { get; set; }
  }
}
