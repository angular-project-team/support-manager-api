﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Models
{
    public class InitiatorRequest
    {
        public int Id { get; set; }

        public string RequestedBy { get; set; }

        public Application AppId { get; set; }

        public SupportAction SupportActionId { get; set; }

        public RequestStatus RequestStatus { get; set; }

        public string AuthorizedBy { get; set; }

        public DateTime RequestDate { get; set; }

        public DateTime AuthorizedDate { get; set; }

        public DateTime PostDate { get; set; }

        public string PayloadValue { get; set; }


    }

    public enum RequestStatus
    {
        Pending,
        Authorized,
        Posted
    }
}