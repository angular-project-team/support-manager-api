﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Models.Users
{
    public class AuthorizerRequest

    {
        public string RequestId { get; set; }

        public string AuthorizedBy { get; set; }

        public DateTime AuthorizedDate { get; set; }

        
    }
}