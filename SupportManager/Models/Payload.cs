﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Models
{
    public class Payload
    {
        public int Id { get; set; }
        public InitiatorRequest RequestId { get; set; }
        public SupportAction SupportActionId { get; set; }
        public string ParameterValue { get; set; }
    }
}