﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using SupportManager.Models;
using SupportManager.Models.SuperAdmin;

namespace SupportManager.Controllers
{
    public class SuperAdminController : ApiController
    {
        [HttpPost]
        [Route("api/superadmin/addadmin")]
        public IHttpActionResult AddAdmin ([FromBody] APIRequest request)
        {
            return Ok<APIResponse>(Services.SuperAdminService.AddAdmin(request));
        }
    }
}
