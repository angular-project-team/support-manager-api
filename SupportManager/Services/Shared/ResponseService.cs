﻿using Newtonsoft.Json;
using SupportManager.Helpers;
using SupportManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupportManager.Services.Shared
{
    public class ResponseService
    {
        private static object response;

        

        public static APIResponse InvalidModelState()
        {
            string RequestId = DateTime.Now.ToString("yyMMddHHmmssfff") + Utilities.RandomId(3);
            APIResponse response = new APIResponse();
            response.ResponseCode = "03";
            response.ResponseMessage = "Model State is invalid";
            ErrorHandler.WriteLog($"Response was sent to the caller|{JsonConvert.SerializeObject(response)}", RequestId);
            return response;
        }

        
    }
}