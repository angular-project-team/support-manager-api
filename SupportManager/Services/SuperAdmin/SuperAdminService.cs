using Newtonsoft.Json;
using SupportManager.Helpers;
using SupportManager.Models;
using SupportManager.Models.SuperAdmin;
using SupportManager.Models.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using APIRequest = SupportManager.Models.SuperAdmin.APIRequest;
using APIResponse = SupportManager.Models.APIResponse;

namespace SupportManager.Services
{
    public class SuperAdminService
    {
        
        public static APIResponse AddAdmin(APIRequest request)
        {
            string RequestId = DateTime.Now.ToString("yyMMddHHmmssfff") + Utilities.RandomId(3);
            APIResponse response = new APIResponse();
            response.ResponseCode = "00";
            response.RequestId = RequestId;
            ErrorHandler.WriteLog($"Request received|{JsonConvert.SerializeObject(request)}", RequestId);

            if (string.IsNullOrEmpty(request.StaffId))
            {
                response.ResponseCode = "03";
                response.ResponseMessage = "Staff Id is empty!";             
                ErrorHandler.WriteLog($"Response was sent to the caller|{JsonConvert.SerializeObject(response)}", RequestId);
            }
            else
            {
                try
                {
                    // Check if user is a Fidelity bank staff
                    string exist = CheckUser.CheckStaffExistence(request.StaffId, RequestId);
                    if (exist == "valid")
                    {
                        // Insert into db
                        Dictionary<string, dynamic> addAdminParams = new Dictionary<string, dynamic>();
                        addAdminParams.Add("@StaffId", request.StaffId);
                        int addAdminDT = Utilities.EnCapriDisco_Insert("SupportManager_AddAdmin", addAdminParams, RequestId, "");
                        response.ResponseCode = "00";
                        
                        if (Math.Abs(addAdminDT) > 0)
                        {
                            response.ResponseMessage = "Admin successfully added!";
                        }
                        else
                        {
                            response.ResponseCode = "01";
                            response.ResponseMessage = "Admin could not be added!";
                        }
                    }
                    else
                    {
                        response.ResponseCode = "03";
                        response.ResponseMessage = "Staff Id is invalid!";
                        ErrorHandler.WriteLog($"Response was sent to the caller|{JsonConvert.SerializeObject(response)}", RequestId);
                    }

                }
                catch (Exception ex)
                {
                    response.ResponseCode = "99";
                    response.ResponseMessage = "Error occured while processing!.";
                    ErrorHandler.WriteError("Error occured while requesting cheque" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, RequestId);
                }
            }           

            ErrorHandler.WriteLog($"Response was sent to the caller|{JsonConvert.SerializeObject(response)}", RequestId);
            return response;
        }
  }
}
