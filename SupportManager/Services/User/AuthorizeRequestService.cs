﻿using Newtonsoft.Json;
using SupportManager.Helpers;
using SupportManager.Models;
using SupportManager.Models.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SupportManager.Services.User
{
    public class AuthorizeRequestService
    {
        public static APIResponse AuthorizeRequest(AuthorizerRequest request, string id)
        {
            string RequestId = DateTime.Now.ToString("yyMMddHHmmssfff") + Utilities.RandomId(3);
            APIResponse response = new APIResponse();
            response.ResponseCode = "00";
            response.RequestId = RequestId;
            ErrorHandler.WriteLog($"Request received|{JsonConvert.SerializeObject(request)}", RequestId);


            try
            {
                request.RequestId = id;

                // Insert into db
                Dictionary<string, dynamic> authorizeRequestParams = new Dictionary<string, dynamic>();
                authorizeRequestParams.Add("@StaffId", request.AuthorizedBy);
                authorizeRequestParams.Add("@AuthorizedDate", DateTime.Now);
                authorizeRequestParams.Add("@RequestId", request.RequestId);
                DataTable authorizeRequestDT = Utilities.EnCapriDisco_Select2("SupportManager_AuthorizeRequest", authorizeRequestParams, RequestId, "");

                if (authorizeRequestDT.Rows[0].ItemArray[0].ToString() == "1")
                {
                    response.ResponseCode = "00";
                    response.ResponseMessage = "Request has been authorized successfully!";
                }
                else
                {
                    response.ResponseCode = "01";
                    response.ResponseMessage = "Request could not be authorized!";
                }


            }
            catch (Exception ex)
            {
                response.ResponseCode = "99";
                response.ResponseMessage = "Error occured while processing!.";
                ErrorHandler.WriteError("Error occured while requesting cheque" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, RequestId);
            }

            ErrorHandler.WriteLog($"Response was sent to the caller|{JsonConvert.SerializeObject(response)}", RequestId);
            return response;
        }
    }
}