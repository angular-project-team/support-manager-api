using Newtonsoft.Json;
using SupportManager.Helpers;
using SupportManager.Models;
using SupportManager.Models.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using APIResponse = SupportManager.Models.APIResponse;

namespace SupportManager.Services
{
    public class UserService
    {

        public static RequestResponse ViewPendingRequest(PendingRequestRequest request)
      {
        string RequestId = DateTime.Now.ToString("yyMMddHHmmssfff") + Utilities.RandomId(3);
        RequestResponse response = new RequestResponse();
        response.ResponseCode = "00";
        response.RequestId = RequestId;
        ErrorHandler.WriteLog($"Request received|{JsonConvert.SerializeObject(request)}", RequestId);

        if (string.IsNullOrEmpty(request.StaffId))
        {
          response.ResponseCode = "03";
          response.ResponseMessage = "Staff Id is empty!";
          ErrorHandler.WriteLog($"Response was sent to the caller|{JsonConvert.SerializeObject(response)}", RequestId);
        }
        else
        {
          try
          {
            // Check if user is a Fidelity bank staff
            string exist = CheckUser.CheckStaffExistence(request.StaffId, RequestId);

            switch (exist)
            {
              case "valid":

                // Get Pending Request
                Dictionary<string, dynamic> params1 = new Dictionary<string, dynamic>();
                params1.Add("@staffId", request.StaffId);
                DataTable viewPendingRequestDT = Utilities.EnCapriDisco_Select2("SupportManager_ViewPendingRequest", params1, RequestId, "");
                response.ResponseCode = "00";

                if (viewPendingRequestDT.Rows.Count == 0) // No Pending Request Found!
                {
                  response.ResponseCode = "01";
                  response.ResponseMessage = "No pending request found.";
                }
                else
                {
                  response.ResponseMessage = "List of pending request";
                  response.ResponseObject = viewPendingRequestDT;
                }
                break;
            }

          }
          catch (Exception e)
          {
            response.ResponseCode = "99";
            response.ResponseMessage = "Error occured while processing!.";
            ErrorHandler.WriteError("Error occured while requesting lists" + Environment.NewLine + e.Message + Environment.NewLine + e.StackTrace, RequestId);
          }
        }

        ErrorHandler.WriteLog($"Response was sent to the caller|{JsonConvert.SerializeObject(response)}", RequestId);
        return response;
      }
    }
}
