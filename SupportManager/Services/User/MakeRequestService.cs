﻿using Newtonsoft.Json;
using SupportManager.Helpers;
using SupportManager.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SupportManager.Services.User
{
    public class MakeRequestService
    {
        public static APIResponse MakeRequest(InitiatorRequest request)
        {
            string RequestId = DateTime.Now.ToString("yyMMddHHmmssfff") + Utilities.RandomId(3);
            APIResponse response = new APIResponse();
            response.ResponseCode = "00";
            response.RequestId = RequestId;
            ErrorHandler.WriteLog($"Request received|{JsonConvert.SerializeObject(request)}", RequestId);


            try
            {
                // Insert into db
                Dictionary<string, dynamic> makeRequestParams = new Dictionary<string, dynamic>();
                    makeRequestParams.Add("@StaffId", request.RequestedBy);
                    makeRequestParams.Add("@ApplicationId", request.AppId.Id);
                    makeRequestParams.Add("@SupportActionId", request.SupportActionId.Id);
                    makeRequestParams.Add("@RequestDate", request.RequestDate);
                    makeRequestParams.Add("@PayloadValue", request.PayloadValue);
                   
                    //DataTable addRequestDT = Utilities.EnCapriDisco_Insert("SupportManager_AddRequest", makeRequestParams, RequestId, "");
                    DataTable addRequestDT = Utilities.EnCapriDisco_Select2("SupportManager_AddRequest", makeRequestParams, RequestId, "");
                int Req_Id = 0;
                if (addRequestDT.Rows.Count > 0)
                    //if (Math.Abs(addRequestDT) > 0)
                    {
                        foreach (DataRow item in addRequestDT.Rows)
                        {
                            Req_Id = Convert.ToInt32(item["request_id"].ToString());
                        }
                        Dictionary<string, dynamic> makeRequestParams2 = new Dictionary<string, dynamic>();
                        makeRequestParams2.Add("@RequestId", Req_Id);
                        makeRequestParams2.Add("@SupportActionId", request.SupportActionId.Id);
                        makeRequestParams2.Add("@PayloadValue", request.PayloadValue);
                        int addRequestDT1 = Utilities.EnCapriDisco_Insert("SupportManager_AddPayload", makeRequestParams2, RequestId, "");
                        if (Math.Abs(addRequestDT1) > 0)
                        {
                            response.ResponseCode = "00";
                            response.ResponseMessage = "Payload value successfully added!";
                        }
                        else
                        {

                        response.ResponseCode = "01";
                        response.ResponseMessage = "Payload value could not be added!";
                    }
                    
                    }
                    else
                    {
                        response.ResponseCode = "01";
                        response.ResponseMessage = "Request could not be added!";
                    }
                

            }
            catch (Exception ex)
            {
                response.ResponseCode = "99";
                response.ResponseMessage = "Error occured while processing!.";
                ErrorHandler.WriteError("Error occured while requesting cheque" + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace, RequestId);
            }

            ErrorHandler.WriteLog($"Response was sent to the caller|{JsonConvert.SerializeObject(response)}", RequestId);
            return response;
        }
    }
}