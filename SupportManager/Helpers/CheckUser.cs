﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SupportManager.Helpers
{
    public class CheckUser
    {
        public static bool CheckSuperAdmin(string staffId)
        {
            var result = Utilities.GetConfigByKey("SuperAdminId");
            if(staffId == result)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CheckAdmin(string staffId)
        {


            return true;
        }

        // This method gets its data from our users' table in the db
        public static bool CheckStaff(string staffId)
        {
            return true;
        }

        // This method gets its data from infopool db
        public static string CheckStaffExistence(string staffId, string requestId)
        {
            //check if the user exist on AD and retrieve email address
            Dictionary<string, dynamic> sqlParameters = new Dictionary<string, dynamic>();
            sqlParameters.Add("@StaffId", staffId);
            var status = "";

            try
            {
                DataTable dataTable = Utilities.EnCapriDisco_Select2("SupportManager_CheckUserIsFidelityStaff", sqlParameters, requestId, staffId);
                if (dataTable.Rows[0].ItemArray[0].ToString() != "1") // user does not exist on AD
                {
                    status = "invalid";
                }
                else
                {
                    status = "valid";
                }

            }
            catch (Exception ex)
            {
                ErrorHandler.WriteError("System/Network related error while checking Staff ID" + Environment.NewLine
                                        + "REQUEST ID => " + requestId, "");
                status = "error";
            }

            return status;
        }

    }
}