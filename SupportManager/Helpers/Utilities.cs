﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Globalization;
using System.IO;
using System.Reflection;
using SupportManager.Helpers;

namespace SupportManager.Helpers
{
    public class Utilities
    {
        public Utilities()
        {

        }

        //Checks config by key
        public static string GetConfigByKey(string key)
        {
            if (ConfigurationManager.AppSettings["key"] != null)
            {
                return ConfigurationManager.AppSettings["key"].ToString();
            }

            else return string.Empty;
        }

        public static bool ExistInAppConfigkey(string key, string value, char splitBy)
        {
            string values = ConfigurationManager.AppSettings[key] ?? "";
            string[] valuesSplit = values.Split(splitBy);
            if (valuesSplit.Contains(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string RandomId(int idLength)
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                Enumerable
                   .Range(65, 26)
                    .Select(e => ((char)e).ToString())
                    .Concat(Enumerable.Range(97, 26).Select(e => ((char)e).ToString()))
                    .Concat(Enumerable.Range(0, 10).Select(e => e.ToString()))
                    .OrderBy(e => Guid.NewGuid())
                    .Take(idLength)
                    .ToList().ForEach(e => builder.Append(e));
                return builder.ToString();
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteError("Exception error occurred at RandomId: " + ex.Message, "");
                return ex.Message.Substring(idLength);
            }
        }

    internal static DataTable EnCapriDisco_Select2(string v1, object params1, string requestId, string v2)
    {
      throw new NotImplementedException();
    }

    public static string RandomGuid()
        {
            return Guid.NewGuid().ToString();
        }

        public static bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format.
            return Regex.IsMatch(strIn,
                    @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
        }

        public static bool isNumeric(string str)
        {
            return Regex.IsMatch(str, "^[0-9]+$");
        }

        public static bool IsNumericForwardSlash(string str)
        {
            return Regex.IsMatch(str, "^[0-9/]+$");
        }

        public static bool IsNumericDot(string str)
        {
            return Regex.IsMatch(str, "^[0-9.]+$");
        }

        public static bool IsValidPostingAmount(string str)
        {
            if (Convert.ToDecimal(str) <= 0)
            {
                return false;
            }

            if (isNumeric(str))
            {
                return true;
            }
            else if (IsNumericDot(str))
            {
                if (str.Substring(str.IndexOf(".")).Length - 1 == 2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool IsValidName(string str)
        {
            return Regex.IsMatch(str, @"^[\p{L}\p{M} \.\-]+$");
        }

        public static bool IsValidDate(string date, string format)
        {
            DateTime datetxt;
            try
            {
                datetxt = DateTime.ParseExact(date, format, null);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string SHA512(string value) //normal SHA512
        {
            var signature = string.Empty;

            SHA512 shaM = new SHA512Managed();

            byte[] messageBytes = Encoding.UTF8.GetBytes(value);
            byte[] hash = shaM.ComputeHash(messageBytes);
            for (int i = 0; i < hash.Length; i++)
            {
                signature += hash[i].ToString("x2"); // hex format
            }
            return signature;
        }

        public static string SHA512withKey(string stringToSign, string apiSecret)
        {
            string hex = string.Empty;
            byte[] secretkeyBytes = Encoding.UTF8.GetBytes(apiSecret);
            byte[] inputBytes = Encoding.UTF8.GetBytes(stringToSign);

            using (var hmac = new HMACSHA512(secretkeyBytes))
            {
                byte[] hashValue = hmac.ComputeHash(inputBytes);
                hex = BitConverter.ToString(hashValue);
                hex = hex.Replace("-", "");
                hex = hex.ToLower();
            }
            return hex;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            char[] buffer = new char[str.Length];
            int idx = 0;

            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')
                    || (c >= 'a' && c <= 'z') || (c == '.') || (c == '_') || (c == ' ') || (c == '&') || (c == '-'))
                {
                    buffer[idx] = c;
                    idx++;
                }
            }

            return new string(buffer, 0, idx);
        }

        public static string GetIP()
        {
            string strHostName = string.Empty;
            string ipaddress = string.Empty;

            try
            {
                strHostName = Dns.GetHostName();

                IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);

                ipaddress = Convert.ToString(ipEntry.AddressList[0]);

                if (ipaddress.Contains(":"))
                {
                    ipaddress = Convert.ToString(ipEntry.AddressList[1]);
                }
            }
            catch (Exception ip)
            {
                ErrorHandler.WriteError("Error getting IP address: " + ip.Message, "");
                ipaddress = ip.Message.Replace("'", "");
            }
            return ipaddress.ToString();
        }

        public static string GetCustomerIP(string requestid)
        {
            string CustomerIPs = string.Empty;
            string[] CustomerIP1;
            string CustomerIP = string.Empty;
            try
            {
                if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
                {
                    CustomerIPs = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    CustomerIPs = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
                }

                if (CustomerIPs.Contains(","))
                {
                    CustomerIP1 = CustomerIPs.Split(',');
                    CustomerIP = CustomerIP1[0];
                    ErrorHandler.WriteError("IP address and ISP IP: " + CustomerIPs, requestid);
                }
                else
                {
                    CustomerIP = CustomerIPs;
                    ErrorHandler.WriteError("IP address and ISP IP: " + CustomerIPs, requestid);
                }
            }
            catch (Exception ex)
            {
                CustomerIP = "";
                ErrorHandler.WriteError("Error in GetCustomerIP(): " + ex.Message, requestid);
            }

            return CustomerIP;
        }

        public class objectMultiSelect
        {
            public string[] sItem;
            public string[] sValue;
        }

        public static Int32 RandomNumber(Int32 Low, Int32 High)
        {
            Random rndNum = new Random(int.Parse(Guid.NewGuid().ToString().Substring(0, 8), NumberStyles.HexNumber));
            return rndNum.Next(Low, High);
        }


        public static DataTable ListToDataTable<T>(List<T> items, string requestid)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            try
            {
                //Get all the properties
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Defining type of data column gives proper data table 
                    var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name, type);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteError("Exception error occured in GenUtilities.ListToDataTable: " + ex.Message + " " + ex.StackTrace, requestid);
                dataTable = null;
            }

            return dataTable;
        }

        public static string WriteByteArrayToFile(string inPDFByteArrayStream, string filelocation)
        {
            try
            {
                byte[] data = Convert.FromBase64String(inPDFByteArrayStream);

                //pdflocation = pdflocation + fileName;   // file name should come with |.pdf| extension

                using (FileStream Writer = new FileStream(filelocation, FileMode.Create, FileAccess.Write))
                {

                    Writer.Write(data, 0, data.Length);
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return filelocation;
        }

        public static DataTable EnCapriDisco_Select(dynamic parameterValue, string requestId, string procedureName, string parameterName)
        {
            DataTable dtRecord = new DataTable();
            SqlDataAdapter dtadapt = new SqlDataAdapter();
            string classMethod = "EnCapriDisco_Select";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    SqlCommand comm = new SqlCommand(procedureName, conn);
                    comm.CommandType = CommandType.StoredProcedure;
                    comm.Parameters.AddWithValue(parameterName, parameterValue);
                    //comm.Parameters.AddWithValue("@Amount", amount);

                    dtadapt.SelectCommand = comm;
                    dtadapt.Fill(dtRecord);
                    //conn.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, requestId);
            }
            return dtRecord;
        }

        public static DataTable EnCapriDisco_Select2(string procedureName, Dictionary<string, dynamic> sqlParameters, string callMethod, string userId)
        {
            DataTable dtRecord = new DataTable();
            SqlDataAdapter dtadapt = new SqlDataAdapter();
            string classMethod = "EnCapriDisco_Select";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    SqlCommand comm = new SqlCommand(procedureName, conn);
                    comm.CommandType = CommandType.StoredProcedure;
                    foreach (var item in sqlParameters)
                    {
                        comm.Parameters.AddWithValue(item.Key, item.Value);
                    }

                    dtadapt.SelectCommand = comm;
                    dtadapt.Fill(dtRecord);
                    //conn.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteError(classMethod + " unable to connect to DB. " + ex.Message, userId);
            }
            return dtRecord;
        }


        public static int EnCapriDisco_Insert(string procedureName, Dictionary<string, dynamic> sqlParameters, string callMethod, string userId)
        {
            DataTable dtRecord = new DataTable();
            SqlDataAdapter dtadapt = new SqlDataAdapter();
            int res = 0;
            string classMethod = "EnCapriDisco_Insert " + callMethod;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString()))
                {
                    if (conn.State != ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    SqlCommand comm = new SqlCommand(procedureName, conn);
                    comm.CommandType = CommandType.StoredProcedure;

                    foreach (var item in sqlParameters)
                    {
                        comm.Parameters.AddWithValue(item.Key, item.Value);
                    }



                    try
                    {
                        res = comm.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler.WriteError(classMethod + "Unable to write to db for password " + ex.Message, userId);
                        res = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteError(classMethod + "Unable to connect to db to write password: " + " | " + ex.Message, userId);
            }
            return res;
        }

        public static bool CallerIsAuthorized(string RequestId)
        {

            var classMethod = "Authorization";
            //Authenticate the user credentiatials
            string authHeader = HttpContext.Current.Request.Headers["Authorization"];
            if (authHeader == null || !authHeader.StartsWith("Basic"))
            {
                ErrorHandler.WriteError("caller is not authorized", RequestId);
                return false;
            }
            string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();

            //the coding should be iso or you could use ASCII and UTF-8 decoder
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));
            int seperatorIndex = usernamePassword.IndexOf(':');

            string username = usernamePassword.Substring(0, seperatorIndex);
            string password = usernamePassword.Substring(seperatorIndex + 1);

            if (username != ConfigurationManager.AppSettings["HelloTracks_Username"].ToString().Trim() || password != ConfigurationManager.AppSettings["HelloTracks_Pass"].ToString().Trim())
            {
                ErrorHandler.WriteError(classMethod + "caller is not authorized: " + " | ", RequestId);
                return false;
            }
            ErrorHandler.WriteError(classMethod + "caller is authorized: " + " | ", RequestId);
            return true;
        }


    }
}